/*
 *    Copyright 2014 - 2018 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.echechess.communication.redis.configuration;


import ca.watier.echechess.communication.redis.interfaces.GameRepository;
import ca.watier.echechess.communication.redis.model.GenericGameHandlerWrapper;
import ca.watier.echechess.communication.redis.pojos.ServerInfoPojo;
import ca.watier.echechess.communication.redis.repositories.RedisGameRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;

@Configuration
@Profile("dependent-mode")
public class RedisConfiguration {
    private final ServerInfoPojo redisServerPojo;

    @Autowired
    public RedisConfiguration(@Qualifier("redisServerPojo") ServerInfoPojo redisServerPojo) {
        this.redisServerPojo = redisServerPojo;
    }

    @Bean
    public GameRepository gameRepository() {
        return new RedisGameRepositoryImpl(redisTemplateGenericGameHandlerWrapper());
    }

    @Bean
    public RedisTemplate<String, GenericGameHandlerWrapper> redisTemplateGenericGameHandlerWrapper() {
        RedisTemplate<String, GenericGameHandlerWrapper> template = new RedisTemplate<>();
        template.setConnectionFactory(jedisConnectionFactory());
        return template;
    }

    @Bean
    public JedisConnectionFactory jedisConnectionFactory() {
        return new JedisConnectionFactory(redisStandaloneConfiguration());
    }

    @Bean
    public RedisStandaloneConfiguration redisStandaloneConfiguration() {
        RedisStandaloneConfiguration redisStandaloneConfiguration = new RedisStandaloneConfiguration();
        redisStandaloneConfiguration.setHostName(redisServerPojo.getIp());
        redisStandaloneConfiguration.setPort(redisServerPojo.getPort());
        return redisStandaloneConfiguration;
    }

    @Bean
    public <T> RedisGameRepositoryImpl<T> redisGameRepository(RedisTemplate<String, GenericGameHandlerWrapper<T>> redisTemplate) {
        return new RedisGameRepositoryImpl<>(redisTemplate);
    }
}
