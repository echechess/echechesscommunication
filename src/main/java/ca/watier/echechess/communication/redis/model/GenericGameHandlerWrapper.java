/*
 *    Copyright 2014 - 2018 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.echechess.communication.redis.model;

import ca.watier.echechess.communication.redis.repositories.RedisGameRepositoryImpl;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

import java.io.Serializable;

@RedisHash(RedisGameRepositoryImpl.REDIS_GAME_REPO_KEY)
public class GenericGameHandlerWrapper<T> implements Serializable {
    private static final long serialVersionUID = 7025692131109629747L;
    private final T genericGameHandler;

    @Id
    private String id;

    public GenericGameHandlerWrapper(T genericGameHandler) {
        this.genericGameHandler = genericGameHandler;
    }

    public GenericGameHandlerWrapper(String id, T genericGameHandler) {
        this.genericGameHandler = genericGameHandler;
        this.id = id;
    }

    public T getGenericGameHandler() {
        return genericGameHandler;
    }

    public String getId() {
        return id;
    }
}
