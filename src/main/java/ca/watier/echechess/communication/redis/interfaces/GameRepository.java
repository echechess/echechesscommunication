/*
 *    Copyright 2014 - 2018 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.echechess.communication.redis.interfaces;

import ca.watier.echechess.communication.redis.model.GenericGameHandlerWrapper;

import java.util.List;

public interface GameRepository<T> {
    void add(GenericGameHandlerWrapper<T> game);

    void add(String id, T game);

    void delete(String id);

    GenericGameHandlerWrapper<T> get(final String id);

    List<GenericGameHandlerWrapper<T>> getAll();
}
