/*
 *    Copyright 2014 - 2018 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.echechess.communication.redis.repositories;


import ca.watier.echechess.communication.redis.interfaces.GameRepository;
import ca.watier.echechess.communication.redis.model.GenericGameHandlerWrapper;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.List;

public class RedisGameRepositoryImpl<T> implements GameRepository<T> {
    public static final String REDIS_GAME_REPO_KEY = "GAME_REPO";
    private final HashOperations<String, String, GenericGameHandlerWrapper<T>> hashOperations;

    public RedisGameRepositoryImpl(RedisTemplate<String, GenericGameHandlerWrapper<T>> redisTemplate) {
        if (redisTemplate == null) {
            throw new IllegalArgumentException("The RedisTemplate cannot be null!");
        }

        hashOperations = redisTemplate.opsForHash();
    }

    @Override
    public void add(String id, T game) {
        if (id == null || game == null) {
            throw new IllegalArgumentException("The game or the key cannot be null!");
        }

        add(new GenericGameHandlerWrapper<>(id, game));
    }

    @Override
    public void add(GenericGameHandlerWrapper<T> game) {
        if (game == null || game.getId() == null) {
            throw new IllegalArgumentException("GenericGameHandlerWrapper or the key cannot be null!");
        }

        hashOperations.put(REDIS_GAME_REPO_KEY, game.getId(), game);
    }

    @Override
    public void delete(String id) {
        if (id == null) {
            throw new IllegalArgumentException("The key cannot be null!");
        }

        hashOperations.delete(REDIS_GAME_REPO_KEY, id);
    }

    @Override
    public GenericGameHandlerWrapper<T> get(final String id) {
        if (id == null) {
            throw new IllegalArgumentException("The key cannot be null!");
        }

        return hashOperations.get(REDIS_GAME_REPO_KEY, id);
    }

    @Override
    public List<GenericGameHandlerWrapper<T>> getAll() {
        return hashOperations.values(REDIS_GAME_REPO_KEY);
    }
}
