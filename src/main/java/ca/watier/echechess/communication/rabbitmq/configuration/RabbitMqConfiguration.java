/*
 *    Copyright 2014 - 2018 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.echechess.communication.rabbitmq.configuration;

import ca.watier.echechess.communication.redis.pojos.ServerInfoPojo;
import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("dependent-mode")
public class RabbitMqConfiguration {
    public static final String MOVE_WORK_QUEUE_NAME = "ECHECCHESS_GAME_MOVE_WORK_QUEUE";
    public static final String AVAIL_MOVE_WORK_QUEUE_NAME = "ECHECCHESS_GAME_AVAIL_MOVE_WORK_QUEUE";
    public static final String TOPIC_EXCHANGE_NAME = "ECHECCHESS";
    public static final String ROUTING_KEY_MOVE_NODE_TO_APP = "chess.message.node.to.app.move";
    public static final String ROUTING_KEY_AVAIL_MOVE_NODE_TO_APP = "chess.message.node.to.app.avail.move";

    private final ServerInfoPojo rabbitMqServerPojo;

    @Autowired
    public RabbitMqConfiguration(@Qualifier("rabbitMqServerPojo") ServerInfoPojo redisServerPojo) {
        this.rabbitMqServerPojo = redisServerPojo;
    }

    @Bean
    public Queue moveWorkQueue() {
        return new Queue(MOVE_WORK_QUEUE_NAME);
    }

    @Bean
    public Queue availMoveWorkQueue() {
        return new Queue(AVAIL_MOVE_WORK_QUEUE_NAME);
    }

    @Bean
    public DirectExchange exchange() {
        return new DirectExchange(TOPIC_EXCHANGE_NAME);
    }

    @Bean
    public Binding bindingMove(DirectExchange exchange) {
        return BindingBuilder.bind(nodeToAppMoveQueue()).to(exchange).with(ROUTING_KEY_MOVE_NODE_TO_APP);
    }

    @Bean
    public Queue nodeToAppMoveQueue() {
        return new AnonymousQueue();
    }

    @Bean
    public Binding bindingAvailMove(DirectExchange exchange) {
        return BindingBuilder.bind(nodeToAppAvailMoveQueue()).to(exchange).with(ROUTING_KEY_AVAIL_MOVE_NODE_TO_APP);
    }

    @Bean
    public Queue nodeToAppAvailMoveQueue() {
        return new AnonymousQueue();
    }
}
