A simple project that contains the configuration to allow the modules to communicate via Redis & RabbitMQ

<br/>
<br/>
To setup RabbitMq

```
rabbitmqctl add_user YOUR_USERNAME YOUR_PASSWORD
rabbitmqctl set_user_tags YOUR_USERNAME administrator
rabbitmqctl set_permissions -p / YOUR_USERNAME ".*" ".*" ".*"
```